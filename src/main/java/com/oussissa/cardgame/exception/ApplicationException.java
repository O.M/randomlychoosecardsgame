package com.oussissa.cardgame.exception;

/**
 * Toutes les exceptions qui peuvent arriver de manière générale
 * @author Ousmane
 *
 */
public class ApplicationException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ApplicationException() {
		super();
	}
	
	public ApplicationException(String message){
		super(message);
	}
	
	public ApplicationException(Throwable e){
		super(e);
	}

}
