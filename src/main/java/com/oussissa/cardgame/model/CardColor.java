package com.oussissa.cardgame.model;

public enum CardColor {
	COEUR, TREFLE, CARREAU, PIQUE
}
