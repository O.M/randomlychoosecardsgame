package com.oussissa.cardgame.model;

public enum CardValue {
	EIGHT, AS, TEN, NINE, SEVEN, SIX, FIVE, FOUR, ROI, THREE, DAME, TWO, VALET;
}
