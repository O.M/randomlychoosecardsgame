package com.oussissa.cardgame.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.oussissa.cardgame.exception.ApplicationException;

/**
 * Represente une carte
 * 	Une carte est identifié uniquement par sa position déterminée :
 * 		1. l'ordre de sa couleur (cardColor)
 * 		2. puis l'ordre de sa valeur
 * Donc la position d'une carte parmi les differentes cartes [au nombre de CardColor.values().lengh()*CardValue.values().length()) ]
 *  est  = cardColor.ordinal()*CardValue().values().length() + cardValue.ordinal()
 * @author Ousmane
 *
 */
public class Card implements Comparable<Card>{
	
	private CardColor 	cardColor;
	private CardValue	cardValue;
	
	public CardColor getCardColor() {
		return cardColor;
	}
	public void setCardColor(CardColor cardColor) {
		this.cardColor = cardColor;
	}
	public CardValue getCardValue() {
		return cardValue;
	}
	public void setCardValue(CardValue cardValue) {
		this.cardValue = cardValue;
	}
	
	/**
	 * Retourne une carte à partir d'une position.
	 * @param number
	 * @return
	 * @throws GameCardPositionDoesNotExist
	 */
	public static Card buildCardFromNumber(int number) throws ApplicationException{
		
		//Position maximale
		int boundValue = CardColor.values().length * CardValue.values().length - 1 ;

		//Si le nombre donné n'est pas entre la position minimale et la position maximale alors générer une exception
		if (number < 0 || number > boundValue)
			throw new ApplicationException("Le nombre doit être entre 0 et " + boundValue);
		
		//La carte à générer 
		Card card = new Card();
		
		//Trouver la couleur et la valeur de la carte
		int color = number / CardValue.values().length ;
		int value = number % CardValue.values().length ;
		
		card.setCardColor(CardColor.values()[color]);
		card.setCardValue(CardValue.values()[value]);
		
		return card;
	}
	
	/**
	 * Calcule et retourne la position de la carte parmi toutes les cartes
	 * @return
	 */
	@JsonIgnore
	public int getCardPosition() {
		return CardValue.values().length * cardColor.ordinal() + cardValue.ordinal();
	}
	
	@Override
	public int compareTo(Card o) {
		if ( this.getCardPosition() > o.getCardPosition())
			return  1;
		if ( this.getCardPosition() < o.getCardPosition())
			return -1;
		return 0;
	}
	
	@Override
	public String toString() {
		return "{ cardColor : " + cardColor + ", cardValue : " + cardValue + " }";
	}
}
