package com.oussissa.cardgame.model;

import java.util.List;

public class ResultLists {
	
	private List<Card> orderedList;
	private List<Card> unorderedList;
	
	public List<Card> getOrderedList() {
		return orderedList;
	}
	public void setOrderedList(List<Card> orderedList) {
		this.orderedList = orderedList;
	}
	public List<Card> getUnorderedList() {
		return unorderedList;
	}
	public void setUnorderedList(List<Card> unorderedList) {
		this.unorderedList = unorderedList;
	}
	
	@Override
	public String toString() {
		return "{\n orderedList : " + orderedList + "\n unorderedList : " + unorderedList;
	}
	
}
