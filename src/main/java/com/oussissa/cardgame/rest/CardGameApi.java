package com.oussissa.cardgame.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.oussissa.cardgame.model.Card;
import com.oussissa.cardgame.model.CardColor;
import com.oussissa.cardgame.model.CardValue;
import com.oussissa.cardgame.model.ResultLists;
import com.oussissa.cardgame.service.contrat.IGame;

import jakarta.servlet.http.HttpServletResponse;
/**
 * Controller gérant les API rest du jeu
 * @author Ousmane
 *
 */
@RestController
public class CardGameApi {

	Logger logger = LoggerFactory.getLogger(CardGameApi.class);
	
	@Autowired
	private IGame igame;
	
	@RequestMapping(path = "/random/cards", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResultLists getTenRandomCard(HttpServletResponse response){
		
		logger.debug("Demande de génération de 10 cartes aléatoirement");
		
		ResultLists resultLists = new ResultLists();
		
		try {
			
			List<Card> tenRandomCard 					=	igame.chooseNCards(10);
			List<Card> tenRandomCardOrdered 			=	new ArrayList<>(tenRandomCard);
			tenRandomCardOrdered.sort(null);
			
			resultLists.setOrderedList(tenRandomCardOrdered);
			resultLists.setUnorderedList(tenRandomCard);
			
			response.setStatus(HttpServletResponse.SC_OK);
		}catch(Exception e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			resultLists = new ResultLists() ;
		}
		
		logger.debug("Les 10 cartes générées : " + resultLists);
		
		return resultLists;
	}
	
	@GetMapping("/value/sort/order")
	public List<CardValue> getValueSortOrder(){
		logger.debug("Demande de la liste des valeurs ordonnées");
		return Arrays.asList(CardValue.values());
	}
	
	@GetMapping("/color/sort/order")
	public List<CardColor> getColorSortOrder(){
		logger.debug("Demande de la liste des couleurs ordonnées");
		return Arrays.asList(CardColor.values());
	}
	
}