package com.oussissa.cardgame.service.realisation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.oussissa.cardgame.exception.ApplicationException;
import com.oussissa.cardgame.model.Card;
import com.oussissa.cardgame.model.CardColor;
import com.oussissa.cardgame.model.CardValue;
import com.oussissa.cardgame.service.contrat.IGame;

/**
 * Implémentation de contrat de choix de N nombre
 * @author Ousmane
 *
 */
@Component
public class GameImpl implements IGame{

	Logger logger = LoggerFactory.getLogger(GameImpl.class);
	
	public List<Card> chooseNCards(int n) throws ApplicationException {
		
		if (n == 0)
			return Arrays.asList();
		
		if (n < 0 || n > 52) {
			int total = CardColor.values().length * CardValue.values().length;
			logger.error("Le nombre de cartes à choisir est entre 0 et {}", total);
			throw new ApplicationException("Le nombre de cartes à choisir est entre 0 et " + total );
		}
		
		List<Card> cards = getNRandomCards(n);
		
		return cards;
	}

	/**
	 * Calcul et retourne N cartes aleatoires (avec une distribution aleatoire)
	 * Le principe de l'algorithme est : 
	 *  1.	Au départ on a un maximum et un minimum ce qui nous fait un interval
	 *  2.	Une fois que le premier élément est choisi aléatoirement (uniformement), on divise l'interval en deux parties qui vont contenir que les nombres non choisis d'abord
	 *  3.	Donc nous avons deux listes maintenant
	 *  4.	Le prochain choix aléatoire est fait d'abord parmi les listes puis le prochain élément à choisir est fait dans la liste choisie aléatoirement
	 *  5.	Et cette dernière liste est aussi divisée en deux ...
	 *  6.	Ainsi de suite jusqu'à l'obtention de n éléments choisis aléatoirement
	 * @param number
	 * @return
	 * @throws ApplicationException
	 */
	private List<Card> getNRandomCards(int number) throws ApplicationException {

		List<Card> cardsFound = new ArrayList<Card>();
		List<int[]> intervals = new ArrayList<>();
		intervals.add(new int[] { 0, CardColor.values().length * CardValue.values().length - 1 });

		Random random = new Random();
		int i = 0;
		int position;
		while (i < number) {
			if (intervals.size() == 0)
				break;
			int[] bounds = intervals.remove(random.nextInt(intervals.size()));
			if (bounds[0] == bounds[1])
				position = bounds[0];
			else {
				position = random.nextInt( bounds[1] - bounds[0] + 1 ) + bounds[0];
				
				if (position == bounds[0])
					intervals.add(new int[] { bounds[0] + 1, bounds[1] });
				else {
					if (position == bounds[1])
						intervals.add(new int[] { bounds[0], bounds[1] - 1 });
					else {
						intervals.add(new int[] { bounds[0], position - 1 });
						intervals.add(new int[] { position + 1, bounds[1] });
					}
				}
			}
			cardsFound.add(Card.buildCardFromNumber(position));
			i++;
		}
		
		return cardsFound;
	}
	
}
