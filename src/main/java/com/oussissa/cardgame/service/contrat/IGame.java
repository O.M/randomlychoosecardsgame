package com.oussissa.cardgame.service.contrat;

import java.util.List;

import com.oussissa.cardgame.exception.ApplicationException;
import com.oussissa.cardgame.model.Card;


public interface IGame {
	/**
	 *Choisir aleatoirement n cartes
	 * 	Contrainte :  0 =< n <= CardValue.values().length
	 * @param n
	 * @return
	 * @throws GameRandomNumberOut52
	 */
	List<Card> chooseNCards(int n) throws ApplicationException;
}
