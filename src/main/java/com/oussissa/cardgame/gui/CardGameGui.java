package com.oussissa.cardgame.gui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Le controlleur pour générer l'interface du jeu
 * @author Ousmane
 *
 */
@Controller
public class CardGameGui {

	Logger logger = LoggerFactory.getLogger(CardGameGui.class);
	
	@GetMapping("/")
	public String home() {
		logger.debug("Demande d'accès à l'application du jeux de carte");
		return "index";
	}
	
	@GetMapping("/error")
	public String error(HttpServletResponse response, HttpServletRequest request) {
		return "error";
	}

	
}
