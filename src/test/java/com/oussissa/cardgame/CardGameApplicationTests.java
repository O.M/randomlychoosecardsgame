package com.oussissa.cardgame;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.oussissa.cardgame.exception.ApplicationException;
import com.oussissa.cardgame.model.Card;
import com.oussissa.cardgame.model.CardColor;
import com.oussissa.cardgame.model.CardValue;
import com.oussissa.cardgame.service.contrat.IGame;
import com.oussissa.cardgame.service.realisation.GameImpl;

@SpringBootTest
class CardGameApplicationTests {

	@Test
	public void testCardColorOrder(){
		
		//Témoin
		List<CardColor> trueSortedList 	=	Arrays.asList(CardColor.COEUR, CardColor.TREFLE, CardColor.CARREAU, CardColor.PIQUE);
		
		//Résultat 
		List<CardColor> listToSort 		=	new ArrayList<CardColor>();
		listToSort.add(CardColor.COEUR);
		listToSort.add(CardColor.PIQUE);
		listToSort.add(CardColor.TREFLE);
		listToSort.add(CardColor.CARREAU);
		//calcul
		listToSort.sort(null);
		
		//vérification
		assertEquals(trueSortedList, listToSort);
		
	}

	@Test
	public void testCardValueOrder(){
		
		//Valorisation du temoin
		List<CardValue> trueSortedList = Arrays.asList(CardValue.EIGHT, CardValue.AS, CardValue.TEN, CardValue.NINE, CardValue.SEVEN, CardValue.SIX, CardValue.FIVE, CardValue.FOUR, CardValue.ROI, CardValue.THREE, CardValue.DAME, CardValue.TWO, CardValue.VALET);
		
		//resultat pour comparer
		List<CardValue> listToSort = new ArrayList<CardValue>(); 
		listToSort.add(CardValue.NINE)	;
		listToSort.add(CardValue.EIGHT)	;
		listToSort.add(CardValue.AS)	; 
		listToSort.add(CardValue.TEN)	; 
		listToSort.add(CardValue.SEVEN)	; 
		listToSort.add(CardValue.SIX)	;	 
		listToSort.add(CardValue.DAME)	; 
		listToSort.add(CardValue.TWO)	; 
		listToSort.add(CardValue.VALET)	;
		listToSort.add(CardValue.FOUR)	; 
		listToSort.add(CardValue.FIVE)	;
		listToSort.add(CardValue.ROI)	; 
		listToSort.add(CardValue.THREE)	;
		//calcul
		listToSort.sort(null);
		
		//vérification
		assertEquals(trueSortedList, listToSort);
	}
	
	
	@Test
	public void testNumberCardGenerated() throws ApplicationException {
		
		//initialisation
		IGame game = new GameImpl();
		int numberOfCardsToGenerate 		= 10;
		
		List<Card> 	list = game.chooseNCards(numberOfCardsToGenerate);
		Set<Integer> cardsIds = new HashSet<>();
		for(Card card : list)
			cardsIds.add(card.getCardPosition());
		
		//Evaluer
		assertEquals(numberOfCardsToGenerate, cardsIds.size());
	}
	
	
}
