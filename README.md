# RandomlyChooseCardsGame 
Ceux-ci est un mini projet contenant une partie back en java/springboot et une partie front en reactJs. Il permet tout simplement de générer 10 cartes aleatoirement et les afficher à l'utilisateur dans un ordre bien défini au départ. Cet ordre dépend de la couleur et de la valeur de la carte 

## Versions

- Java          :   java 17 (version 17.0.8)
- maven         :   Apache Maven 3.9.0

## Test and Deploy
Pour tester et déployer
`
1. Cloner le projet
2. Se Placer dans le dossier racine du projet
3. mvn clean install
4. mvn spring-boot:run
5. Accéder à l'application à l'adresse : http://localhost:8989/ dans un navigateur

`
